# MiniC interpreter and typer
LAB3, MIF08 / CAP / CS444 2022-23


# Authors

Ulysse Durand

# Contents

TODO for STUDENTS : Say a bit about the code infrastructure ...

# Howto

`make test-interpret TEST_FILES='TP03/tests/provided/examples/test_print_int.c'` for a single run

`make test` to test all the files in `*/tests/*` according to `EXPECTED` results.

You can select the files you want to test by using `make test TEST_FILES='TP03/**/*bad*.c'` (`**` means
"any number of possibly nested directories").

# Test design 

TODO: explain your tests. Do not repeat what test files already contain, just give the main objectives of the tests.

# Design choices

TODO: explain your choices - explain the limitations of your implementation.

# Known bugs

Not all tests pass

I have reached the end of the deadline during the debug of my type-checker implementation.
