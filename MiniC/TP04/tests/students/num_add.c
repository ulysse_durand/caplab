#include "printlib.h"

int main() {

    println_int(3 + 4);
    println_int(4 - 3);
    return 0;
}
// EXPECTED
// 7
// 1
