# cap-labs 2023: from CAP course, ENSL M1 Students

This project was about making a compiler of the MiniC language (a subset of C) in Python.

Everything comes from here : https://github.com/Drup/cap-lab23

This is a copy of the repository plus what I did in the practicals (MiniC compiler) minus the infos about the exams, grades, etc...


## Course

* Teachers:
  - Gabriel Radanne, Inria, LIP https://gabriel.radanne.net/
  - Ludovic Henrio, CNRS, LIP https://lhenrio.github.io/

* Covid Era's video courses: https://www.youtube.com/playlist?list=PLtjm-n_Ts-J-6EU1WfVIWLhl1BUUR-Sqm

## Contents

   * TPxx/     : student companion files for CAP labs 2023-24

## About the target machine

The target machine is [RISCV](https://riscv.org/).
The directory contains [instructions](INSTALL.md) to install a compiler and a simulator.

## Contact

Gabriel Radanne, Inria, LIP [email](mailto:gabriel.radanne@ens-lyon.fr)


## Contributors (Who made the course and the base repository)

  * Labs (ENSL 2019 version): Laure Gonnord, Ludovic Henrio, Matthieu Moy, Marc de Vismes
  * 2020 : Gabriel Radanne, Paul Iannetta
  * 2021-2023 : Nicolas Chappe, Remi Di Guardia
  * 2023-2024 : Samuel Humeau, Hugo Thievenaz
