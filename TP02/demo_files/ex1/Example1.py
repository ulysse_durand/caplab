# Generated from Example1.g4 by ANTLR 4.11.1
from antlr4 import *
from io import StringIO
import sys
if sys.version_info[1] > 5:
    from typing import TextIO
else:
    from typing.io import TextIO


def serializedATN():
    return [
        4,0,6,36,6,-1,2,0,7,0,2,1,7,1,2,2,7,2,2,3,7,3,2,4,7,4,2,5,7,5,1,
        0,1,0,1,1,1,1,1,2,1,2,1,3,1,3,1,4,1,4,1,4,5,4,25,8,4,10,4,12,4,28,
        9,4,1,5,4,5,31,8,5,11,5,12,5,32,1,5,1,5,0,0,6,1,1,3,2,5,3,7,4,9,
        5,11,6,1,0,4,3,0,42,43,45,45,47,47,1,0,48,57,2,0,65,90,97,122,3,
        0,9,10,13,13,32,32,38,0,1,1,0,0,0,0,3,1,0,0,0,0,5,1,0,0,0,0,7,1,
        0,0,0,0,9,1,0,0,0,0,11,1,0,0,0,1,13,1,0,0,0,3,15,1,0,0,0,5,17,1,
        0,0,0,7,19,1,0,0,0,9,21,1,0,0,0,11,30,1,0,0,0,13,14,7,0,0,0,14,2,
        1,0,0,0,15,16,7,1,0,0,16,4,1,0,0,0,17,18,7,2,0,0,18,6,1,0,0,0,19,
        20,2,40,41,0,20,8,1,0,0,0,21,26,3,5,2,0,22,25,3,5,2,0,23,25,3,3,
        1,0,24,22,1,0,0,0,24,23,1,0,0,0,25,28,1,0,0,0,26,24,1,0,0,0,26,27,
        1,0,0,0,27,10,1,0,0,0,28,26,1,0,0,0,29,31,7,3,0,0,30,29,1,0,0,0,
        31,32,1,0,0,0,32,30,1,0,0,0,32,33,1,0,0,0,33,34,1,0,0,0,34,35,6,
        5,0,0,35,12,1,0,0,0,4,0,24,26,32,1,6,0,0
    ]

class Example1(Lexer):

    atn = ATNDeserializer().deserialize(serializedATN())

    decisionsToDFA = [ DFA(ds, i) for i, ds in enumerate(atn.decisionToState) ]

    OP = 1
    DIGIT = 2
    LETTER = 3
    PARENTHESIS = 4
    ID = 5
    WS = 6

    channelNames = [ u"DEFAULT_TOKEN_CHANNEL", u"HIDDEN" ]

    modeNames = [ "DEFAULT_MODE" ]

    literalNames = [ "<INVALID>",
 ]

    symbolicNames = [ "<INVALID>",
            "OP", "DIGIT", "LETTER", "PARENTHESIS", "ID", "WS" ]

    ruleNames = [ "OP", "DIGIT", "LETTER", "PARENTHESIS", "ID", "WS" ]

    grammarFileName = "Example1.g4"

    def __init__(self, input=None, output:TextIO = sys.stdout):
        super().__init__(input, output)
        self.checkVersion("4.11.1")
        self._interp = LexerATNSimulator(self, self.atn, self.decisionsToDFA, PredictionContextCache())
        self._actions = None
        self._predicates = None


