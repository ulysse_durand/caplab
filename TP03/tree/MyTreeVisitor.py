from TreeVisitor import TreeVisitor
from TreeParser import TreeParser


class UnknownIdentifier(Exception):
    pass


class MyTreeVisitor(TreeVisitor):
    """Visitor that evaluates an expression. Derives and overrides methods
    from ArithVisitor (generated by ANTLR4)."""
    def __init__(self):
        self._memory = dict()  # store id -> values

    def visitTop(self, ctx):
        return self.visit(ctx.int_tree())

    def visitLeaf(self, ctx):
        return True

    def visitNode(self, ctx):
        value = True
        for i in range(ctx.getChildCount()-3):
            value = value and (self.visit(ctx.int_tree(i)))
        return value and (ctx.getChildCount()-3 == 2)
